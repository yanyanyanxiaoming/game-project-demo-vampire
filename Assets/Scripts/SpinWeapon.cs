﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinWeapon : MonoBehaviour
{
    public float firballrotatespeed = 100f;
    public Transform holder;
    public Transform firballtospown;
    private bool firballactive = false;

    public float firballAmount;
    void Update()
    {
        holder.rotation = Quaternion.Euler(0, 0, holder.rotation.eulerAngles.z + (firballrotatespeed * Time.deltaTime));
        if (!firballactive)
        {
            for (int i = 0; i < firballAmount; i++)
            {
                float rot = 360f / firballAmount * i;
                Instantiate(firballtospown, firballtospown.position, Quaternion.Euler(0f,0f, rot), holder).gameObject.SetActive(true);
            }
           
            firballactive = true;
        }
    }
}
