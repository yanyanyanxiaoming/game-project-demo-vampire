﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // Start is called before the first frame update
    public int movespeed=1;
    public static PlayerController instance;

    void Awake()
    {
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 moveinput = new Vector3(0f, 0f, 0f);
        moveinput.x = Input.GetAxisRaw("Horizontal");
        moveinput.y = Input.GetAxisRaw("Vertical");
        moveinput.Normalize();
        transform.position += moveinput * movespeed * Time.deltaTime;
    }
    
}
