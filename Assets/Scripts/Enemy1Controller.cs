﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class Enemy1Controller : MonoBehaviour
{
    private Rigidbody2D enemy1rb;
    public float enemy1movespeed=0.3f;
    public float health = 200f;
    public float knockbacktime = 0.2f;
    private float knockbackcount;
    public static EnemySpown enemyspown;


    private Transform playertarget;
    private Vector3 positivelocalScale = new Vector3(1, 1, 1);
    private Vector3 nagativelocalScale = new Vector3(-1, 1, 1);




    void Start()
    {
        enemy1rb = GetComponent<Rigidbody2D>();
        playertarget = FindObjectOfType<PlayerController>().transform;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 direction = playertarget.position - transform.position;
        if (direction.x<0)
        {
            transform.localScale = positivelocalScale;
        }
        else
        {
            transform.localScale = nagativelocalScale;
        }
        enemy1rb.velocity = (playertarget.position - transform.position).normalized * enemy1movespeed;

        if (knockbackcount>0)
        {
            knockbackcount -= Time.deltaTime;
            if (enemy1movespeed>0)
            {
                enemy1movespeed = -enemy1movespeed*3f;
            }
            if (knockbackcount<=0)
            {
                enemy1movespeed=Mathf.Abs(enemy1movespeed/3f);
            }
        }


        
    }

    public void TakeDamage(float damagetotake)
    {
        health -= damagetotake;
        if (health < 0) 
        {
            Destroy(gameObject);
        }
    }

    public void TakeDamage(float damagetotake,bool shoudknockback)
    {
        TakeDamage(damagetotake);
        if (shoudknockback)
        {
            knockbackcount = knockbacktime;
        }
    }
}
