﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpown : MonoBehaviour
{
    // Start is called before the first frame update
    public float timespown=2f;
    private float spownCounter;
    public int enemysMaxCount = 80;
    public int enemysInstanceCount;
    public GameObject enemyToSpown;
    public Transform minSpown, maxSpown;
    private Transform playertarget;


    void Start()
    {
        playertarget = PlayerController.instance.transform;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = playertarget.position;
        spownCounter -= Time.deltaTime;
        if (spownCounter<=0)
        {
            spownCounter = timespown;
            GameObject newEnemy = Instantiate(enemyToSpown, selectSpownPoint(), transform.rotation);
        }
    }

    private Vector3 selectSpownPoint()
    {
        Vector3 spownpoint = Vector3.zero;
        bool spownVerticalEdge = UnityEngine.Random.Range(0f, 1f) > 0.5f;
        if (spownVerticalEdge)
        {
            spownpoint.y = UnityEngine.Random.Range(minSpown.position.y, maxSpown.position.y);
            if (UnityEngine.Random.Range(0f,1f)>0.5f)
            {
                spownpoint.x = maxSpown.position.x;
            }
            else
            {
                spownpoint.x = minSpown.position.x;
            }
        }
        else
        {
            spownpoint.y = UnityEngine.Random.Range(minSpown.position.x, maxSpown.position.x);
            if (UnityEngine.Random.Range(0f, 1f) > 0.5f)
            {
                spownpoint.y = maxSpown.position.y;
            }
            else
            {
                spownpoint.y = minSpown.position.y;
            }
        }


        return spownpoint;
    }
}
