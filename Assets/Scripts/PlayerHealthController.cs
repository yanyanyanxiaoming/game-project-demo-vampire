﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealthController : MonoBehaviour
{

    private float currentHealth;
    public float maxHealth;

    void Start()
    {
        currentHealth = maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            TakeDamage(10f);
            if (currentHealth>0)
            {
                print("当前剩余血量" + currentHealth);
            }
            
        }
    }

    private void TakeDamage(float damage)
    {
        
        currentHealth -= damage;
        if(currentHealth<=0)
        {
            gameObject.SetActive(false);
            Destroy(gameObject);
            print("角色阵亡");
        }
        
    }
}
