﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.WSA;

public class EnemyDamager : MonoBehaviour
{

    public float damgeamount=100f;
    public float timeBetweenDamage;
    public float weaponLifeTime=5f;
    private float damageCounter;


    public bool shoudknockback;
    public bool isOvertimeWeapon;
    public bool isPinWeapon;

    
    private List<Enemy1Controller> enmeyisInrange= new List<Enemy1Controller>();
    




    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (isOvertimeWeapon)
        {
            if (collision.CompareTag("Enemy"))
            {
                enmeyisInrange.Add(collision.GetComponent<Enemy1Controller>());
            }
        }
        else
        {
            if (collision.CompareTag("Enemy"))
            {
                collision.GetComponent<Enemy1Controller>().TakeDamage(damgeamount, shoudknockback);
                print("Enemy");

                if (isPinWeapon)
                {
                    Destroy(gameObject);
                }
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (isOvertimeWeapon)
        {
            if (collision.CompareTag("Enemy"))
            {
                enmeyisInrange.Remove(collision.GetComponent<Enemy1Controller>());
            }
        }
    }

    private void Update()
    {
        if (isOvertimeWeapon)
        {
            damageCounter -= Time.deltaTime;
            
            if (damageCounter<=0)
            {
                damageCounter = timeBetweenDamage;
                for (int i = 0; i < enmeyisInrange.Count; i++)
                {
                    if (enmeyisInrange[i]!=null)
                    {
                        enmeyisInrange[i].TakeDamage(damgeamount, shoudknockback);
                    }
                    else
                    {
                        enmeyisInrange.RemoveAt(i);
                        i--;
                    }
                }

            }
        }
        if (isPinWeapon)
        {
            weaponLifeTime -= Time.deltaTime;
            if (weaponLifeTime < 0)
            {
                Destroy(gameObject);
            }
        }
    }

}
