﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

public class projectileweapon : MonoBehaviour
{
    public Projectil projectil;
    public float shootTime = 2f;
    private float shootCount;

    public float weaponRange;
    public LayerMask isEnemy;
    public int danggerAmount = 1;

    private void Update()
    {
        shootCount -= Time.deltaTime;
        if (shootCount<=0)
        {
            shootCount = shootTime;
            Collider2D[] enemis = Physics2D.OverlapCircleAll(transform.position, weaponRange * 10, isEnemy);
            if (enemis.Length>0)
            {
                for (int i = 0; i < danggerAmount; i++)
                {
                    Vector3 targetposition = enemis[Random.Range(0, enemis.Length)].transform.position;

                    Vector3 dirction = targetposition - transform.position;

                    float angle = Mathf.Atan2(dirction.y,dirction.x)*Mathf.Rad2Deg;
                    angle -= 90;

                    projectil.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

                    Instantiate(projectil, PlayerController.instance.transform.position, projectil.transform.rotation).gameObject.SetActive(true); 
                }
            }
        }
    }


}
